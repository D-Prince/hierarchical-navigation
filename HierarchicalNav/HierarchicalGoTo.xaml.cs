﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HierarchicalNav
{
    public partial class HierarchicalGoTo : ContentPage
    {
        public HierarchicalGoTo(string getWords)
        {
            InitializeComponent();
            GoTo.Text = getWords;

        }
    }
}
